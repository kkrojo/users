import React, {useEffect} from 'react'
import {Form, Input, Button, Row, Col, message} from 'antd'
import { UserOutlined, PhoneOutlined, MailOutlined, CalendarOutlined} from '@ant-design/icons'
import logo from "../utils/img.png";

import axios from "axios";
import LABELS from "../labels";
import { RouteComponentProps } from "react-router-dom";

interface RouteParams {
    id: string
}

interface Props extends RouteComponentProps<RouteParams> {
}

const SignupPage: React.FC<Props> = (props) => {
    const [form] = Form.useForm();


    useEffect(() => {
        if (props.match.params.id) {

            axios.get(`/api/users/${props.match.params.id}`)
                .then((res) => {
                    let newData = {...res.data.user}
                    console.log(newData)
                    form.setFieldsValue({ registerForm : newData })
                })
                .catch(function (error) {
                    message.error(LABELS.ERROR_MESSAGE);
                    console.log(error)
                });
        }
    },[props.match.params.id])


    const validateMessages = {
        // eslint-disable-next-line no-template-curly-in-string
        required: '${label} الزامی است!',
        types: {
            // eslint-disable-next-line no-template-curly-in-string
            number: '${label} یک شماره صحیح نیست، اعداد انگلیسی وارد کنید!!',
        },
        number: {
            // eslint-disable-next-line no-template-curly-in-string
            range: '${label} باید بین ${min} و ${max} باشد!',
            // eslint-disable-next-line no-template-curly-in-string
            min: '${label} نباید زیر ${min} باشد! '
        },
    };
    const onFinish = async (values : any) => {
        try {
            axios.post(`/api/users`, values.registerForm, ).then(res => {
                props.history.push('/users')
            })

        } catch (err) {
            console.log(err)
        }
    }

    return (
        <div className={'register-container'}>
            <Row justify='center' >
                <Col  md={{ span:11 }} lg={{ span:8 }} sm={{ span:18 }} xs={{ span:22 }}>
                    <div className=" text-center logo-container">
                        <img src={logo} width={100} alt=" logo"/>
                    </div>
                    <div className="form-container">
                        <div className={'form-header'}>

                            <div className={'inline-block'}>
                                <h3>{LABELS.FILL_THE_FORM}</h3>
                            </div>
                        </div>
                        <Form
                            name='registerForm'
                            onFinish={onFinish}
                            layout='vertical'
                            form={form}
                            validateMessages={validateMessages}
                        >
                            <Form.Item
                                name={['registerForm', 'name']}
                                label={LABELS.NAME}
                                rules={[{
                                    required: true,
                                }]}
                            >
                                <Input
                                    prefix={<UserOutlined
                                        className='site-form-item-icon'
                                    />}
                                    placeholder={LABELS.NAME}
                                />
                            </Form.Item>

                            <Form.Item
                                name={['registerForm', 'phone']}
                                label={LABELS.PHONE_NUMBER}
                                rules={[{
                                    required: true,
                                }]}
                            >
                                <Input
                                    prefix={<PhoneOutlined
                                        className='site-form-item-icon'
                                    />}
                                    placeholder={LABELS.PHONE_NUMBER}
                                />
                            </Form.Item>
                            <Form.Item
                                name={['registerForm', 'age']}
                                label={LABELS.AGE}
                                rules={[{ required: true,
                                }
                                ]}
                            >
                                <Input
                                    prefix={<CalendarOutlined
                                        className='site-form-item-icon'
                                    />}
                                    placeholder={LABELS.AGE}


                                />
                            </Form.Item>

                            <Form.Item
                                name={['registerForm', 'email']}
                                label={LABELS.EMAIL}
                                rules={[{
                                    required: true,
                                    type: 'email'
                                }
                                ]}

                            >
                                <Input
                                    prefix={<MailOutlined
                                        className='site-form-item-icon'
                                    />}
                                    placeholder={LABELS.EMAIL}


                                />
                            </Form.Item>

                            <Button style={{
                                background: "#DE2D26",
                                borderRadius: "4px",
                                color:"#fff",
                                height: '48px',
                                width: '100%'}} htmlType='submit'>
                                {LABELS.REGISTER}
                            </Button><br/>
                        </Form>
                    </div>
                </Col>
            </Row>
        </div>
    )
}
export default SignupPage;