import React, { useEffect, useState} from 'react';
import {Button, Col, message, Modal} from "antd";
import LABELS from "../labels";
import {DeleteOutlined, PlusCircleOutlined, EditOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import axios from "axios";

import { Link } from 'react-router-dom';
import CustomTable from "../components/table/customTable";
import logo from "../utils/img.png";

const Users: React.FC = () => {
    const { confirm } = Modal;
    let showConfirm = (record : any) => {
        confirm({
            title: LABELS.ARE_YOU_SURE_TO_DELETE,
            icon: <ExclamationCircleOutlined />,
            content: record.instagramPageAddress,
            onOk() {
                deleteUser(record.id, record.name)
            },
        });
    }

    let deleteUser = (id : any, name: string) => {
        axios.delete(`/api/users/${id}`)
            .then(() => {
                // @ts-ignore
                const arr = users.filter((item : any) => item.id !== id);
                setUsers(arr);
                message.success(`${name} با موفقیت حذف شد!`);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    const columns = [
        {
            title: LABELS.NAME,
            dataIndex: 'name',
            key: 'name',
            action:'search',
        },
        {
            title: LABELS.PHONE_NUMBER,
            dataIndex: 'phone',
            key: 'phone',
            action:'search',
            responsive: ["sm"]
        },
        {
            title: LABELS.EMAIL,
            dataIndex: 'email',
            key: 'email',
            action:'search',
            responsive: ["sm"],
        },
        {
            title: LABELS.AGE,
            dataIndex: 'age',
            key: 'age',
            action:'search',
        },
        {
            title: '',
            key: 'name',
            // eslint-disable-next-line react/display-name
            render: (text : string, record : any, index : any) => (<div>
                <Link to={`/edit/${record.id}`} >
                    <EditOutlined  className={'mx-2'} />
                </Link>
                    <DeleteOutlined style={{
                        color: '#DE2D26',
                        marginRight: '20px'
                    }} onClick={() => showConfirm(record)} className={'mx-2'}/>
            </div>)
        },


    ];
    const [users, setUsers] = useState();

    const getUsers = async (): Promise<any> =>{
        const res = await axios.get(`/api/users`)
        // @ts-ignore
        const { data } = await res
        return data;

    }

    useEffect(() => {

        getUsers().then( (result) => {
            let newData = [...result.users];
            console.log(newData)
            newData.forEach((item : any) => (
                // eslint-disable-next-line
                item.key = item.id
            ));
            // @ts-ignore
            setUsers(newData)
        });
    }, [])

    return (
        <div className={'text-center'}>
            <Col  md={{ span:16 }}>

            <div className=" text-center logo-container">
                <img src={logo} width={100} alt=" logo"/>
            </div>
                <div className={'table-buttons-container'}>
                <Button style={{
                    background: "#DE2D26",
                    borderRadius: "4px",
                color:"#fff"}}>
                    <Link to={'/'}>
                        <PlusCircleOutlined />
                        <span>  ایجاد کاربر</span>
                    </Link>
                </Button>
                    <Button style={{
                        color: '#DE2D26',
                        border: '1px solid #DE2D26',
                        borderRadius: '4px'
                    }}>
                    <Link to={'/information'}>
                        دریافت اطلاعات از سرور
                    </Link>
                </Button>
                </div>
            <CustomTable  columns={columns}  data={users}/>
            </Col>
        </div>
    );

};

export default Users;

