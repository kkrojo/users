import React, { useEffect, useState} from 'react';
import { Col} from "antd";
import LABELS from "../labels";
import axios from "axios";
// @ts-ignore
import moment from 'moment-jalali'

import { Link } from 'react-router-dom';
import CustomTable from "../components/table/customTable";
import logo from "../utils/img.png";

const Information: React.FC = () => {


    const columns = [
        {
            title: LABELS.NAME,
            dataIndex: 'name',
            key: 'name',
            action:'search',
        },
        {
            title: LABELS.PHONE_NUMBER,
            dataIndex: 'phone',
            key: 'phone',
            action:'search',
            responsive: ["sm"]
        },
        {
            title: LABELS.EMAIL,
            dataIndex: 'email',
            key: 'email',
            action:'search',
            responsive: ["sm"],
        },




    ];
    const [information, setInforamtion] = useState();

    const getUsers = async (): Promise<any> =>{
        const res = await axios.get(`/api/infos`)
        console.log(res)
        // @ts-ignore
        const { data } = await res
        return data;

    }

    useEffect(() => {

        getUsers().then( (result) => {
            let newAds = [...result.infos];
            newAds.forEach((item : any) => (
                // eslint-disable-next-line
                item.key = item.id,
                item.date = moment(item.date, 'YYYY/M/D').format('jYYYY/jM/jD')

            ));
            // @ts-ignore
            setInforamtion(newAds)
        });
    }, [])

    return (
        <div className={'text-center'}>
            <Col  md={{ span:16 }}>

                <div className=" text-center logo-container">
                    <img src={logo} width={100} alt=" logo"/>
                </div>
                <CustomTable  columns={columns}  data={information}/>
            </Col>
        </div>
    );

};

export default Information;

