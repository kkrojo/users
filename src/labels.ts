const LABELS = {
  FILL_THE_FORM: 'فرم زیر را پر کنید',
  LOGOUT: 'خروج',
  PLEASE_INTER_YOUR_NAME: 'لطفا نام و نام خانوادگی خود را وارد کنید!',
  PLEASE_INTER_YOUR_PHONE_NUMBER: 'لطفا شماره تلفن خود را وارد کنید!',
  PLEASE_INTER_YOUR_PASSWORD: 'لطفا رمز عبور خود را وارد کنید!',
  NAME: 'نام و نام خانوادگی',
  LOGIN: 'ورود',
  AGE: 'سن',
  SEARCH: 'جستجو',
  RESET: 'بازنشانی',
  FILTER: 'فیلتر',
  EMAIL: 'ایمیل',
  PHONE_NUMBER: 'شماره تلفن',
  PASSWORD: 'رمز عبور',
  INACTIVE: 'غیر فعال',
  REGISTER: 'ثبت نام',
  ARE_YOU_SURE_TO_SAVE: 'آیا درباره ذخیره این مورد مطمئن هستید؟',
  ARE_YOU_SURE_TO_DELETE: 'آیا درباره حذف این رکورد مطمئن هستید؟',
  EDIT: 'ویرایش',
  DATE: 'تاریخ',
  SUCCESS_MESSAGE: 'با موفقیت انجام شد!',
  ERROR_MESSAGE: 'مشکلی پیش آمده است!'
}

export default LABELS
